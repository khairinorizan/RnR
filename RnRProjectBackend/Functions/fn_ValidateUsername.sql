USE RnRProjectDb;
GO

IF OBJECT_ID (N'dbo.fnValidateUsername', N'FN') IS NOT NULL
    DROP FUNCTION fnValidateUsername;
GO

/*
 ==================================================
 Author: Muhammad Khairi Norizan
 Date Authored: August 18 2021
 Description: Function to validate username
 ==================================================
*/
CREATE FUNCTION dbo.fnValidateUsername(@username VARCHAR(250))
-- 0 is invalid username
-- 1 is valid username
RETURNS BIT
AS
    BEGIN
        -- Check for whitespace
        IF (SELECT CHARINDEX(' ', @username)) > 0
        BEGIN
            RETURN 0
        END

        -- Check for special characters
        IF ((SELECT PATINDEX('%[^a-zA-Z0-9.-]%', @username)) > 0)
        BEGIN
            RETURN 0
        END

        RETURN 1;
    END
GO