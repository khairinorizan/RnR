USE RnRProjectDb;
GO

IF OBJECT_ID (N'dbo.fnValidatePassword', N'FN') IS NOT NULL
    DROP FUNCTION fnValidatePassword;
GO

/*
 ==================================================
 Author: Muhammad Khairi Norizan
 Date Authored: August 18 2021
 Description: Function to validate password
 ==================================================
*/

CREATE FUNCTION dbo.fnValidatePassword(@password VARCHAR(50))
-- 0 is invalid password
-- 1 is valid password
RETURNS BIT
AS
    BEGIN
        -- Check for password length
        IF (SELECT LEN(@password)) < 8
        BEGIN
            RETURN 0
        END

        -- Check for numeric characters
        IF ((SELECT PATINDEX('%[0-9]%', @password)) = 0)
        BEGIN
            RETURN 0
        END

        -- Check for lower case characters
        IF ((SELECT PATINDEX('%[a-z]%', @password)) = 0)
        BEGIN
            RETURN 0
        END

        -- Check for upper case characters
        IF ((SELECT PATINDEX('%[A-Z]%', @password)) = 0)
        BEGIN
            RETURN 0
        END

        -- Check for special characters
        IF ((SELECT PATINDEX('%[^a-zA-Z0-9.-]%', @password)) = 0)
        BEGIN
            RETURN 0
        END

        RETURN 1
    END
GO