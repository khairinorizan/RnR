USE RnRProjectDb;
GO

IF OBJECT_ID (N'dbo.fnValidateName', N'FN') IS NOT NULL
    DROP FUNCTION fnValidateName;
GO

/*
 ==================================================
 Author: Muhammad Khairi Norizan
 Date Authored: August 18 2021
 Description: Function to validate name string
 ==================================================
*/

CREATE FUNCTION dbo.fnValidateName(@name VARCHAR(50))
-- 0 is invalid name string
-- 1 is valid name string
RETURNS BIT
AS
    BEGIN
        -- Check for whitespace
        IF ((SELECT CHARINDEX(' ', @name)) > 0)
        BEGIN
            RETURN 0
        END

        -- Check for non-alphabet characters
        IF ((SELECT PATINDEX('%[^a-zA-Z]%', @name)) > 0)
        BEGIN
            RETURN 0
        END

        RETURN 1
    END
GO