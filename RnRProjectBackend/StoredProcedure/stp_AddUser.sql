USE RnRProjectDb;
GO

/*
 ==================================================
 Author: Muhammad Khairi Norizan
 Date Authored: August 18 2021
 Description: Store Procedure to add a new user
 ==================================================
*/
CREATE PROCEDURE dbo.stpAddUser
    @LastName VARCHAR(250),
    @FirstName VARCHAR(250),
    @DOB DATE,
    @PhoneNumber VARCHAR(11),
    @Username VARCHAR(50),
    @UserPassword VARCHAR(50),
    @IsHost BIT = 0
AS
BEGIN
    -- SET NOCOUNT ON is added to prevent extra result sets from
    -- interfering with SELECT statements
    SET NOCOUNT ON;

    SET @isValidUsername BIT = SELECT COUNT(Username)
                               FROM RnRUser
                               WHERE Username = @Username

    IF @isValidUsername > 0
    BEGIN
        RETURN 0;
    END

    DECLARE @IsValidUsername BIT;
    DECLARE @IsValidPassword BIT;
    DECLARE @IsValidLastName BIT;
    DECLARE @IsValidFirstName BIT;

    SET @IsValidUsername = dbo.fnValidateUsername(@Username);

    IF @IsValidUsername < 1
    BEGIN
        RETURN 0;
    END

    SET @IsValidPassword = dbo.fnValidatePassword(@UserPassword);

    IF @IsValidPassword < 1
    BEGIN
        RETURN 0;
    END

    IF (SELECT DATEDIFF(YEAR, @DOB, GETDATE())) < 18
    BEGIN
        RETURN 0;
    END

    -- TODO: Validate Last Name
    SET @IsValidLastName = dbo.fnValidateName(@LastName);

    IF @IsValidLastName < 1
    BEGIN
        RETURN 0;
    END

    -- TODO: Validate First Name
    SET @IsValidFirstName = dbo.fnValidateName(@FirstName);

    IF @IsValidFirstName < 1
    BEGIN
        RETURN 0;
    END

    -- TODO: Validate Phone Number

    INSERT INTO dbo.RnRUser
    (LastName, FirstName, DOB, PhoneNumber, Username, UserPassword, IsHost)
    VALUES
    (@LastName, @FirstName, @DOB, @PhoneNumber, @Username, @UserPassword, @IsHost)

    RETURN 1
END
GO