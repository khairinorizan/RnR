USE RnRProjectDb;
GO

CREATE TABLE RnRUser (
    UserId INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
    LastName VARCHAR(250) NOT NULL,
    FirstName VARCHAR(250) NOT NULL,
    DOB DATE NOT NULL,
    PhoneNumber VARCHAR(11),
    Username VARCHAR(50) NOT NULL,
    UserPassword VARCHAR(50) NOT NULL,
    IsHost BIT NOT NULL
);