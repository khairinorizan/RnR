USE RnRProjectDb;
GO

DECLARE @testName1 AS VARCHAR = 'John%%Labu';
DECLARE @testName2 AS VARCHAR = 'JohnLabu';
DECLARE @testName3 AS VARCHAR = 'John-Labu';
DECLARE @testName4 AS VARCHAR = 'JohnLabu009';
DECLARE @testName5 AS VARCHAR = ' JohnLabu009 ';
DECLARE @testName6 AS VARCHAR = 'John.Labu009';

DECLARE @expected1 BIT = (SELECT dbo.fnValidateUsername(@testName1));
DECLARE @expected2 BIT = (SELECT dbo.fnValidateUsername(@testName2));
DECLARE @expected3 BIT = (SELECT dbo.fnValidateUsername(@testName3));
DECLARE @expected4 BIT = (SELECT dbo.fnValidateUsername(@testName4));
DECLARE @expected5 BIT = (SELECT dbo.fnValidateUsername(@testName5));
DECLARE @expected6 BIT = (SELECT dbo.fnValidateUsername(@testName6));

EXEC tSQLt.AssertEquals @expected1, 0, '@expected1 is expecting 0'
EXEC tSQLt.AssertEquals @expected2, 0, '@expected2 is expecting 1'
EXEC tSQLt.AssertEquals @expected3, 0, '@expected3 is expecting 1'
EXEC tSQLt.AssertEquals @expected4, 0, '@expected4 is expecting 1'
EXEC tSQLt.AssertEquals @expected5, 0, '@expected5 is expecting 0'
EXEC tSQLt.AssertEquals @expected6, 0, '@expected6 is expecting 1'