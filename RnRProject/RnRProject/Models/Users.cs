using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using MySqlConnector;

namespace RnRProject.Models
{
    public class Users
    {
        public int userID { get; set; }
        public string lastName { get; set; }
        public string firstName { get; set; }
        public string DOB { get; set; }
        public string username { get; set; }
        public string userPassword { get; set; }
        public string phoneNumber { get; set; }

        internal AppDb Db { get; set; }

        public Users()
        {
        }

        internal Users(AppDb db)
        {
            Db = db;
        }

        public async Task InsertAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"INSERT INTO Users ('LastName', 'FirstName', 'DOB', 'Username', 'UserPassword', 
                     'PhoneNumber') VALUES (@lastName, @firstName, @dob, @userName, @userPassword, @phonenumber);";

            BindParams(cmd);

            await cmd.ExecuteNonQueryAsync();

            // Track the current inserted id
            userID = (int) cmd.LastInsertedId;
        }
        
        public async Task UpdateAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"UPDATE Users SET 'LastName' = @lastName, 'FirstName' = @firstName, 
                   'Username' = @userName, 'UserPassword' = @userPassword, 'PhoneNumber' = @phonenumber WHERE 
                   'UserID' = @id;";
            
            BindParams(cmd);
            BindId(cmd);
            
            await cmd.ExecuteNonQueryAsync();
        }
        
        public async Task DeleteAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM Users WHERE 'UserID' = @id;";
            
            BindId(cmd);
            await cmd.ExecuteNonQueryAsync();
        }
        
        public async Task<List<Users>> FindAll()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Users;";

            return await ReadAllAsync(await cmd.ExecuteReaderAsync());
        }
        
        private async Task<List<Users>> ReadAllAsync(DbDataReader reader)
        {
            var users = new List<Users>();

            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var user = new Users(Db)
                    {
                        userID = reader.GetInt32(0),
                        lastName = reader.GetString(1),
                        firstName = reader.GetString(2),
                        DOB = reader.GetDateTime(3).ToString("dd/MM/yyyy"),
                        username = reader.GetString(4),
                        userPassword = reader.GetString(5),
                        phoneNumber = reader.GetString(6),
                    };
                    
                    users.Add(user);
                }
            }
            return users;
        }
        
        private void BindId(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                DbType = DbType.Int32,
                Value = userID,
            });
        }

        private void BindParams(MySqlCommand cmd)
        {
            // Add parameter last name
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@lastName",
                DbType = DbType.String,
                Value = lastName,
            });
            
            // Add parameter first name
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@FirstName",
                DbType = DbType.String,
                Value = firstName,
            });
            
            // Add parameter dob
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@dob",
                DbType = DbType.String,
                Value = DOB,
            });
            
            // Add parameter Username
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@userName",
                DbType = DbType.String,
                Value = username,
            });
            
            // Add parameter user password
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@userPassword",
                DbType = DbType.String,
                Value = userPassword,
            });
            
            // Add parameter phone number
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@phonenumber",
                DbType = DbType.String,
                Value = phoneNumber,
            });
        }
    }
}