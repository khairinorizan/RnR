using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using MySqlConnector;

namespace RnRProject.Models
{
    public class HostListing
    {
        public int ListID { get; set; }
        public string HostID { get; set; }
        public string HostUsername { get; set; }
        public string HostEmail { get; set; }
        public string PlaceOffering { get; set; }
        public string PlaceDescription { get; set; }
        public string SpaceType { get; set; }
        public int SpacePrice { get; set; }
        public string Address { get; set; }
        public string ListTitle { get; set; }
        public int GuestCount { get; set; }
        public int BedCount { get; set; }
        public int BedroomsCount { get; set; }
        public int BathroomsCount { get; set; }
        public string Amenity { get; set; }
        public string SafetyItems { get; set; }
        public string DescriptionHighlight { get; set; }

        internal AppDb Db { get; set; }

        public HostListing()
        {
        }

        internal HostListing(AppDb db)
        {
            Db = db;
        }

        public async Task InsertAsync()
        {
            using var cmd = Db.Connection.CreateCommand();
            
            
        }
        
        private void BindId(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@ListId",
                DbType = DbType.Int32,
                Value = ListID,
            });
        }
        
        private void BindParams(MySqlCommand cmd)
        {
            // Add parameter HostID
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@HostId",
                DbType = DbType.Int32,
                Value = HostID,
            });
            
            // Add parameter HostUsername
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@HostUsername",
                DbType = DbType.String,
                Value = HostUsername,
            });
            
            // Add parameter HostEmail
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@HostEmail",
                DbType = DbType.String,
                Value = HostEmail,
            });
            
            // Add parameter PlaceOffering
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@PlaceOffering",
                DbType = DbType.String,
                Value = PlaceOffering,
            });
            
            // Add parameter PlaceDescription
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@PlaceDescription",
                DbType = DbType.String,
                Value = PlaceDescription,
            });
            
            // Add parameter SpaceType
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@SpaceType",
                DbType = DbType.String,
                Value = SpaceType,
            });
            
            // Add parameter SpacePrice
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@SpacePrice",
                DbType = DbType.Int32,
                Value = SpacePrice,
            });
            
            // Add parameter Address
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Address",
                DbType = DbType.String,
                Value = Address,
            });
            
            // Add parameter ListTitle
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@ListTitle",
                DbType = DbType.String,
                Value = ListTitle,
            });
            
            // Add parameter GuestCount
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@GuestCount",
                DbType = DbType.Int32,
                Value = GuestCount,
            });
            
            // Add parameter BedCount
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@BedCount",
                DbType = DbType.Int32,
                Value = BedCount,
            });
            
            // Add parameter BedroomsCount
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@BedroomsCount",
                DbType = DbType.Int32,
                Value = BedroomsCount,
            });
            
            // Add parameter BathroomsCount
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@BathroomsCount",
                DbType = DbType.Int32,
                Value = BathroomsCount,
            });
            
            // Add parameter Amenity
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Amenity",
                DbType = DbType.String,
                Value = Amenity,
            });
            
            // Add parameter SafetyItems
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@SafetyItems",
                DbType = DbType.String,
                Value = SafetyItems,
            });
            
            // Add parameter DescriptionHighlight
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@DescriptionHighlight",
                DbType = DbType.String,
                Value = DescriptionHighlight,
            });
        }
    }
}