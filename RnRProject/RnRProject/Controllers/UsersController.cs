using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RnRProject.Models;

namespace RnRProject.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        public AppDb Db { get; }
        
        public UsersController(AppDb db)
        {
            Db = db;
        }
        
        // Get api/users
        [HttpGet]
        public async Task<IActionResult> GetLatest()
        {
            await Db.Connection.OpenAsync();
            var query = new Users(Db);
            var result = await query.FindAll();

            return new OkObjectResult(result);
        }
    }
}