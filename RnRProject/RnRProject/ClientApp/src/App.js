import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home/Home';
import { FetchData } from './components/FetchData';
import { Counter } from './components/Counter';
import HostHome from './components/BecomeHost/HostHome'
import HowRnRWorks from "./components/About/HowRnRWorks";
import ListingRegistration from "./components/ListingRegistration/ListingRegistration"

import './custom.css'

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
        <Route exact path='/' component={Home} />
        <Route path='/Host/Home' component={HostHome} />
        <Route path='/fetch-data' component={FetchData} />
        <Route path='/HowRnRWorks' component={HowRnRWorks} />
      </Layout>
    );
  }
}
