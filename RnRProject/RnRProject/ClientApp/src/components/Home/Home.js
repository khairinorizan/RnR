import React, { Component } from 'react';
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import '../../custom.css';

export class Home extends Component {
  static displayName = Home.name;

  render () {
    return (
        <div>
            <Row className="home-background">
                {/* Search Form */}
                <div className="form-panel">
                    <div className="form-inside-panel">
                        <div className="location-form">
                            <div>
                                <div className="location-inside-form">
                                    <label className="location-label">
                                        <div className="location-label-box">
                                            <div className="location-text">Location</div>
                                            <input class="location-input" aria-autocomplete="list" aria-expanded="false" autoComplete="off" autoCorrect="off" 
                                                   spellCheck="off" role="combobox" placeholder="Where are you going" required="" value=""></input>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div className="right-border-container">
                            
                        </div>
                        <div className="checkIn-checkOut-container">
                            <div className="checkin-container">
                                <div className="checkin-inside-container" role="button" tabIndex="0" aria-expanded="false">
                                    <div className="checkin-dates-container">
                                        <div className="checkin-label">Check in</div>
                                        <div className="checkin-date-label">Add dates</div>
                                    </div>
                                </div>
                            </div>
                            <div className="right-border-container">
                                
                            </div>
                            <div className="checkout-container">
                                <div className="checkout-inside-container" role="button" tabIndex="0" aria-expanded="false">
                                    <div className="checkout-dates-container">
                                        <div className="checkout-label">Check out</div>
                                        <div className="checkout-date-label">Add dates</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="right-border-container">

                        </div>
                        <div className="guest-container">
                            <div className="guest-inside-container" role="button" tabIndex="0" aria-expanded="false">
                                <div className="guest-addguest-container">
                                    <div className="guest-container">Guests</div>
                                    <div className="addguest-container">Add guests</div>
                                </div>
                            </div>
                            <div className="search-button-container">
                                <button className="search-button" aria-expanded="false" type="button">
                                    <div className="search-icon-container">
                                        <div>
                                            <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" style={{display: 
                                            "block", fill: "none", height: "16px", width: "16px", stroke: "currentColor", 
                                            strokeWidth: "4px", overflow: "visible"}} aria-hidden="true" role="presentation"
                                                 focusable="false">
                                                <g fill="none">
                                                    <path d="m13 24c6.0751322 0 11-4.9248678 11-11 
                                                    0-6.07513225-4.9248678-11-11-11-6.07513225 0-11 4.92486775-11 11 0 
                                                    6.0751322 4.92486775 11 11 11zm8-3 9 9">
                                                        
                                                    </path>
                                                </g>
                                            </svg>
                                        </div>
                                        <div className="search-label">Search</div>
                                    </div>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                
                {/* Slogan Label */}
                <div className="slogan-container">
                    <div className="slogan-text">Get with us and Get away.</div>
                </div>
            </Row>
        </div>
    );
  }
  
}

const HomePageProperty = {
    width: '100%',
    height: '100%',
    margin: 'auto'
}

