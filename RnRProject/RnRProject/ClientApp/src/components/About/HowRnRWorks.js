import React, { Component } from "react";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import FooterMenu from '../FooterMenu.js'

export default class HowRnRWorks extends Component{
    static displayName = HowRnRWorks.name;

    constructor(props) {
        super(props);
    }
    
    render(){
        return(
            <div>
                <div className="host-body">
                    <div className="host-body-container-1">
                        <Row>
                            <Col>
                                <div className="about-rnr-header-text-container">
                                    You're just 3 steps away from <br/> your next gateaway
                                </div>
                            </Col>
                        </Row>
                        <br/>
                        <Row>
                            <Col>
                                <div className="about-rnr-body-1-col">
                                    1. Browse
                                </div>
                                <br/>
                                <div>
                                    Start by exploring Stays or Experiences. Apply filters like entire
                                    homes, self check-in, or pets allowed to narrow your options. You can
                                    also save favorites to a wishlist.
                                </div>
                            </Col>
                            <Col>
                                <div className="about-rnr-body-1-col">
                                    2. Book
                                </div>
                                <br/>
                                <div>
                                    Once you have found what you are looking for, learn about your host, read
                                    past guest reviews, and get the details on cancellation options - then book
                                    in just a few clicks.
                                </div>
                            </Col>
                            <Col>
                                <div className="about-rnr-body-1-col">
                                    3. Go
                                </div>
                                <br/>
                                <div>
                                    You are all set! Connect with your host through the app for local tips, questions
                                    or advice. You can also contact RnR anytime for additional support.
                                </div>
                            </Col>
                        </Row>
                    </div>
                    <br/>
                    <hr size="8" width="90%" color="red"/>
                    <div className="host-body-container-1">
                        <Row>
                            <Col>
                                <div className="about-rnr-header-text-container">
                                    Wherever you go, we're here to help
                                </div>
                            </Col>
                        </Row>
                        <br/>
                        <Row>
                            <Col>
                                <div className="about-rnr-body-1-col">
                                    Health and Safety is a priority
                                </div>
                                <br/>
                                <div>
                                    Hosts are commiting to enhanced COVID-19 cleaning protocols, and listings are
                                    rated for cleanliness.
                                </div>
                            </Col>
                            <Col>
                                <div className="about-rnr-body-1-col">
                                    More cancellation options
                                </div>
                                <br/>
                                <div>
                                    Hosts can offer a range of flexible cancellation options which are clearly stated
                                    at booking.
                                </div>
                            </Col>
                            <Col>
                                <div className="about-rnr-body-1-col">
                                    Support anytime, day or night
                                </div>
                                <br/>
                                <div>
                                    With 24/7 global customer support, we are there for you whenever you need assistance.
                                </div>
                            </Col>
                        </Row>
                    </div>
                    <br/>
                    <hr size="8" width="90%" color="red"/>
                    <div className="host-body-container-1">
                        <Row>
                            <Col>
                                <div className="about-rnr-header-text-container">
                                    Frequently Asked Question (FAQ)
                                </div>
                            </Col>
                        </Row>
                        <br/>
                        <Row>
                            <Col>
                                <div className="about-rnr-body-1-col">
                                    Do I need to meet my host?
                                </div>
                                <br/>
                                <div>
                                    Options like self check-in or booking an entire home allow you to interact with your
                                    host mainly through in-app messaging - you can message them anytime if something comes
                                    up
                                </div>
                            </Col>
                            <Col>
                                <div className="about-rnr-body-1-col">
                                    What's RnR doing about COVID-19?
                                </div>
                                <br/>
                                <div>
                                    Get the latest info on our COVID-19 response and resources for guests, including policy
                                    updates, travel restrictions, flexible travel options, and more.
                                </div>
                            </Col>
                            <Col>
                                <div className="about-rnr-body-1-col">
                                    What if I need to cancel due to a problem with the listing or host?
                                </div>
                                <br/>
                                <div>
                                    In most cases, you can resolve any issues directly by messaging your host. If they
                                    can't help, simply contact RnR within 24 hours of discovering the issue.
                                </div>
                            </Col>
                        </Row>
                        <br/>
                        <Row>
                            <Col>
                                <div className="about-rnr-body-1-col">
                                    When am I charged for a reservation?
                                </div>
                                <br/>
                                <div>
                                    You will be charged as soon as your reservation is confirmed, but we hold payment to
                                    your host until 24 hours after check-in to give you time to ensure everything is
                                    expected.
                                </div>
                            </Col>
                            <Col>
                                <div className="about-rnr-body-1-col">
                                    How do I become a host?
                                </div>
                                <br/>
                                <div>
                                    Almost anyone can be a host of a stay or an experience. It's free to sign up and share
                                    either your space or your skills with the world. To get started, visit our Host Center.
                                </div>
                            </Col>
                            <Col>
                                <div className="about-rnr-body-1-col">
                                    Need more information?
                                </div>
                                <br/>
                                <div>
                                    Visit our Help Center to get additional answers to your questions.
                                </div>
                            </Col>
                        </Row>
                    </div>
                </div>
                <FooterMenu/>
            </div>
        )
    }
}

