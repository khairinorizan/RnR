import React, { Component } from 'react';
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

import FooterMenu from '../FooterMenu.js'
import { Link } from 'react-router-dom';

export default class HostHome extends Component {
    static displayName = HostHome.name;
    
    constructor(props) {
        super(props);
    }
    
    render(){
        return(
            <div>
                {/* Top Header */}
                <Row className="host-header">
                    <div className="host-header-text-container">
                        <div className="host-header-text">Become a host.</div>
                        <div className="host-header-text-1">A space to share, <br/> a world to gain</div>
                        <div className="host-header-text-2">
                            Hosting can help you turn your extra space into <br/> 
                            extra income and pursue more of what you love.
                        </div>
                    </div>
                </Row>

                <div className="host-body">
                    <div className="host-body-container-1">
                        <Row>
                            <Col xs={4}>
                                <div className="host-body-1-col1">
                                    Your next chapter, <br/> made possible by <br/> <strong style={{fontStyle: "italic", fontSize: "76px"}}>Hosting</strong>
                                </div>
                            </Col>
                            <Col>
                                <div className="host-body-1-col2">
                                    Welcome what's next
                                </div>
                                <br/>
                                <div>
                                    Enjoy the flexibility of being your own boss, earn extra income, and make 
                                    lifelong connections through hosting.
                                </div>
                                <br/>
                                <br/>
                                <div>
                                    <strong><a href="">Explore the world of hosting</a></strong>
                                </div>
                            </Col>
                            <Col>
                                <div className="host-body-1-col2">
                                    Host with confidence
                                </div>
                                <br/>
                                <div>
                                    From 24/7 support and our helpful Host community, to custom tools, insights, and 
                                    education, we are invested in your success.
                                </div>
                                <br/>
                                <br/>
                                <div>
                                    <strong><a href="">How we support Hosts</a></strong>
                                </div>
                            </Col>
                        </Row>
                    </div>

                    <hr size="8" width="90%" color="red"/>
                    
                    <div className="host-body-container-1">
                        <Row>
                            <Col xs={8}>
                                <h1 className="host-body-title">Why host on RnR?</h1>
                                <h5>Hosts reveal what they love about sharing their space on RnR</h5>
                                <br/>
                                Whatever your financial goals, hosting on RnR offers a unique way to meet them - whether 
                                it's to help pay your mortgage, save for upgrades, or set money aside for vacation. And 
                                the rewards for opening your home go beyond your bank account. Hosts say the perks also 
                                include being able to share your culture and connect with people you would not have met 
                                otherwise, like professional musicians, drone makers, and circus performers.
                            </Col>
                            <Col>
                                <div className="host-body-1-col2">
                                    Highlights
                                </div>
                                <br/>
                                <ul>
                                    <li>
                                        People host on RnR for lots of reasons, such as earning extra money or filling 
                                        a home when kids move out.
                                    </li>
                                    <br/>
                                    <li>
                                        Many have found that they love hosting because they have been able to meet new 
                                        people and experience new cultures.
                                    </li>
                                </ul>
                            </Col>
                        </Row>
                    </div>

                    <hr size="8" width="90%" color="red"/>
                    
                    <div className="host-body-container-1">
                        <h1 className="host-body-title">How to get started on RnR?</h1>
                        <h5>From creating your listing to prepping your space, learn how to start hosting.</h5>
                        <br/>
                        <Row>
                            <Col>
                                <div className="host-body-1-col2">
                                    Create your listing
                                </div>
                                <br/>
                                <h6>
                                    Think of your listing as an advertisement for your space. You will want to make it 
                                    as compelling as possible, while being honest about any quirks.
                                </h6>
                                <br/>
                                <ul>
                                    <li>
                                        <strong>Start with the basics.</strong> Enter details like the location of your 
                                        place, what type of property you are offering, and the number of bedrooms and 
                                        bathrooms your guests will have access to.
                                    </li>
                                    <br/>
                                    <li>
                                        <strong>Take photos of the space.</strong> Guests love browsing photos when making 
                                        a decision on where to stay. To snap the best photos, tidy up your space beforehand. 
                                        Take shots of each area, using natural light and landscape orientation when possible.
                                    </li>
                                    <br/>
                                    <li>
                                        <strong>Highlight unique details.</strong> When writing your listing title and 
                                        description, consider what makes your place special, such as a view or a pool. Also, 
                                        note any aspects in your description that might be important for guests to know 
                                        before booking, like stairs or parking.
                                    </li>
                                </ul>
                            </Col>
                            <Col>
                                <div className="host-body-1-col2">
                                    Organize the logistics
                                </div>
                                <br/>
                                <h6>
                                    Your next step will be lining up all the logistics for your listing to make the hosting 
                                    process run smoothly.
                                </h6>
                                <br/>
                                <ul>
                                    <li>
                                        <strong>Add House Rules to your listing.</strong> To help guests understands your 
                                        expectations, <strong>add rules for your space,</strong> including details like 
                                        restrictions on smoking, pets, or parties.
                                    </li>
                                    <br/>
                                    <li>
                                        <strong>Set up your calendar.</strong> To make sure you only get reservations when 
                                        you are able to host, <strong>update your RnR calendar</strong> with your availability. 
                                        You can also get specific about how much advance notice you need or how far in advance 
                                        guests can book.
                                    </li>
                                    <br/>
                                    <li>
                                        <strong>Choose your nightly price.</strong> What you charge is always up to you, but 
                                        RnR has tools - like <strong>Smart Pricing</strong> - to help you match your prices 
                                        with demand, along with <strong>custom pricing controls</strong> for times like 
                                        weekends and specific seasons.
                                    </li>
                                    <br/>
                                    <li>
                                        <strong>Review your local laws.</strong> Some cities have rules covering home sharing, 
                                        like limits on the number of nights you can host, registration requirements, or special 
                                        taxes.
                                    </li>
                                </ul>
                            </Col>
                            <Col>
                                <div className="host-body-1-col2">
                                    Prepare your space
                                </div>
                                <br/>
                                <h6>
                                    Whether you are expecting your first guest or your 100th, these are the steps you 
                                    will need to take to ensure your space is ready to go.
                                </h6>
                                <br/>
                                <ul>
                                    <li>
                                        <strong>Tidy up.</strong> Clean each room that guests can access, especially 
                                        bedrooms, bathrooms, and the kitchen. Check that there is no hair, dust, or mold 
                                        on surfaces and floors, and make the bed(s) with fresh linens.
                                    </li>
                                    <br/>
                                    <li>
                                        <strong>Store your valuables.</strong> If you have jewelry, passports, or other 
                                        valuables, consider storing them in a locked room, closet, safe, or storage facility. 
                                        Or you can leave them with family or friends.
                                    </li>
                                    <br/>
                                    <li>
                                        <strong>Stock up on essentials.</strong> To help guests feel at home, consider providing 
                                        amenities like soap, shampoo, toilet paper, bed linens, and towels. It cannot hurt 
                                        to have extras on hand as well.
                                    </li>
                                    <br/>
                                    <li>
                                        <strong>Provide check-in details.</strong> Be prepared to check guests in and out, 
                                        or recruit friends or family to help. If no one will be around, you can always use a lockbox 
                                        or electronic lock and provide guests with <strong>check-in instructions</strong> in the 
                                        RnR app.
                                    </li>
                                    {/*<br/>*/}
                                    {/*<li>*/}
                                    {/*    <strong>Add the finishing touches.</strong> Guests love thoughtful details. A house manual */}
                                    {/*    with instructions and tips can help orient guests. A bottle of wine or small gift can also */}
                                    {/*    make guests feel extra welcom, but definitely is not required.*/}
                                    {/*</li>*/}
                                </ul>
                            </Col>
                        </Row>
                    </div>

                    <hr size="8" width="90%" color="red"/>
                    
                    <div className="host-body-container-1">
                        <h1 className="host-body-title">How to earn money on RnR?</h1>
                        <h4>Here's what every host needs to know about pricing and payouts.</h4>
                        <h6>
                            Whether you have a spare room, an extra guest suite, or an entire home sitting vacant, RnR lets 
                            you connect with people from across the globe while <strong>earning revenue from your place.</strong> 
                             But how do you decide what to charge for hosting and how to get paid? Here's what you need to know. 
                        </h6>
                        <br/>
                        <Row>
                            <Col>
                                <div className="host-body-1-col2">
                                    You'll get paid after check-in
                                </div>
                                <br/>
                                <div>
                                    RnR typically releases your payout approximately <strong>24 hours after your guest's scheduled check-in time.</strong> 
                                    For more information on payouts, you can visit our <storng>Payments Terms of Service.</storng> You can also 
                                    check the status of your payouts at any time by reviewing your <strong>transaction history.</strong>
                                </div>
                                <br/>
                                <div>
                                    <strong>How you get paid</strong> is up to you. Available <strong>payout methods</strong> are based on 
                                    your location and can include direct deposit to your bank, international wire, PayPal, Western Union, 
                                    reloadable prepaid cards, and more.
                                </div>
                            </Col>
                            <Col>
                                <div className="host-body-1-col2">
                                    It's free to list your place
                                </div>
                                <br/>
                                <div>
                                    Signing up for RnR and listing your plae is completely free. Simply create an engaging listing
                                    and wait for the reservations to come in. A <strong>host service fee*</strong> will be deducted
                                    from each payout you receive. Fees make RnR possible, and help us provide our hosts with
                                    community support, marketing efforts, host protections, and product development. The payout
                                    you will receive is what you charge guest minus the host service fee.
                                </div>
                            </Col>
                        </Row>
                        <br/>
                        <Row>
                            <Col>
                                <div className="host-body-1-col2">
                                    Local taxes may apply
                                </div>
                                <br/>
                                <div>
                                    Some cities require host to <strong>collect taxes on their RnR</strong> earnings. You'll 
                                    need to check with your local government or other <strong>hosts in your community</strong> 
                                    about whether or not taxes must be collected from guests.
                                </div>
                                <br/>
                                <div>
                                    How you collect any required taxes will vary depending on your location. Some hosts have 
                                    to <strong>collect taxes manually,</strong> while others can <strong>add taxes to their listing</strong> 
                                    or use the RnR <strong>collect and remit feature.</strong> We will let you know what's required for your 
                                    area and provide resourcs to help you get set up. We recomment explaining any taxes you're required to 
                                    collect in your listing description and communications with guests where appropriate.
                                </div>
                            </Col>
                            <Col>
                                <div className="host-body-1-col2">
                                    What you charge is up to you
                                </div>
                                <br/>
                                <div>
                                    When hosting on RnR, you decide the nightly rate your guest will pay. Your rates can be modified
                                    at any time and can vary from night to night, season to season. For example, some hosts charge
                                    a higher nightly rate on weekends and a lower one during the week, when fewer guests are traveling
                                    and more place may be available. Keep in mind, a <strong>guest service fee</strong> will
                                    be added to your price.
                                </div>
                                <br/>
                                <div>
                                    You can also <strong>add fees</strong> for things like extra guests and house cleaning. It's
                                    important to consider these upfront costs when developing your pricing strategy, as cleaning supplies
                                    and everyday essentials like toilet paper can add up. Our <strong>savvy pricing tools</strong> and tips
                                    for creating a <strong>hosting business plan</strong> can help you set a competitiv, profitable price.
                                </div>
                            </Col>
                        </Row>
                    </div>

                    {/* Line break */}
                    <hr size="8" width="90%" color="red"/>
                    
                    <div className="host-body-container-1">
                        <h1 className="host-body-title">Is my space a good fit for RnR?</h1>
                        <h4>There's a perfect guest for every space - the key is setting guest expectations.</h4>
                        <h6>
                            You may not think a cozy sailboat or an apartment above a downtown music venue could be popular 
                            on RnR, but in fact, many different kinds of spaces appeal to guests - even the unique or modest 
                            ones. The trick is creating a detailed, honest listing that showcases what makes your space appealing, 
                            whether it's luxurious, quirky, or simple and affordable.
                        </h6>
                        <br/>
                        <Row>
                            <Col>
                                <div className="host-body-1-col2">
                                    Start with the basics
                                </div>
                                <br/>
                                <div>
                                    What do you need for an RnR space? At a minimum, guests expect a clean, comfortable sleeping 
                                    space and access to a restroom. Not all listings have access to a kitchen, but it is important 
                                    to indicate whether or not your guest will have access to a cooking space.
                                </div>
                            </Col>
                            <Col>
                                <div className="host-body-1-col2">
                                    Define your property type
                                </div>
                                <br/>
                                <div>
                                    RnR spaces run the gamut, so it is important to indicate in your listing the exact type of property 
                                    that you are offering. Is it a house? An apartment? A bed and breakfast or boutique hotel? Some 
                                    listings are even designated "unique spaces" - like treeehouses, yurts, campsites, sailboats, windmills, 
                                    RVs, etc.
                                </div>
                            </Col>
                            <Col>
                                <div className="host-body-1-col2">
                                    Choose where guests can access
                                </div>
                                <br/>
                                <div>
                                    You can indicate to guests that they'll have private access to your entire property, to a private room, 
                                    or that they'll be sharing spaces like the sleeping area, kitchen, or restroom with other people 
                                    such as your family, roommates, or fellow guests. Whether you dedicate the space to guests or keep your 
                                    belongings there is up to you. The important things are to keep your space clean and to communicate with 
                                    guests about exactly what to expect.
                                </div>
                            </Col>
                        </Row>
                        <br/>
                        <Row>
                            <Col>
                                <div className="host-body-1-col2">
                                    Show and Tell
                                </div>
                                <br/>
                                <div>
                                    Clarity is key here: mentioning pets in a listing is a great start, but pictures are worth a thousand 
                                    words, as some guests might book without reading everything thoroughly. If your space includes unique 
                                    features (like a pet), it's also a good idea to confirm during the booking process that guests have 
                                    read the listing thoroughly.
                                </div>
                            </Col>
                            <Col>
                                <div className="host-body-1-col2">
                                    Price your space realistically
                                </div>
                                <br/>
                                <div>
                                    So your space isn't the Taj Mahal - no problem! Many guests are pleased to stay in a modest space 
                                    as long as it feels like a good value. For new hosts, consider starting with a price that's a litte bit 
                                    lower than your ultimate goal. This will help you attract guests, and once you've gotten a handful of 
                                    great reviews, you can re-evaluate and raise your price if you need to.
                                </div>
                            </Col>
                            <Col>
                                <div className="host-body-1-col2">
                                    A space for everyone
                                </div>
                                <br/>
                                <div>
                                    Anyone who has a spare space to share can thrive as an RnR host. There's no such thing as an 
                                    "ideal" listing - you need to be honest and detailed in your description and photos to help 
                                    guests understand exactly what to expect. From modest spare rooms to quirky rustic retreats 
                                    to luxury estates, there's a perfect space for everyone on RnR.
                                </div>
                            </Col>
                        </Row>
                    </div>

                    {/* Line break */}
                    <hr size="8" width="90%" color="red"/>
                    
                    <div className="host-body-container-1">
                        <h1 className="host-body-title">What hosting regulations apply to you?</h1>
                        <h4>Use this guide to navigate local rules and regulations around RnR hosting.</h4>
                        <br/>
                        <h6>
                            Deciding to become a host is an important decision, which is why RnR wants to help you understand 
                            the laws in your area and help clarify guidelines on permits and taxes.
                            <br/>
                            <br/>
                            Because these laws can sometimes be confusing, we partner with local governments to help them better 
                            understand the benefits hosting can bring to their communities. We also advocate for favorable 
                            home-sharing laws at all levels of government to help protect the future of your hosting business.
                        </h6>
                        <br/>
                        <Row>
                            <Col>
                                <div className="host-body-1-col2">
                                    What local rules and regulations apply to you?
                                </div>
                                <br/>
                                <div>
                                    How does your local governments regulate short-term rentals? WHat taxes do RnR hosts in your area
                                    need to pay, and who calculates and collects them? Does your HOA board allow RnR guests in the
                                    building?
                                </div>
                                <br/>
                                <div>
                                    Depending on where in the world you live, there can be requirements at the country, state, city, and
                                    even property level. For instance, some areas require that RnR host register or acquire a license,
                                    and insurance or safety certifications might also be required. Some cities require hosts to pay
                                    specific taxes on their RnR income, while others have laws that restrict short-term rentals.
                                    Local governments can vary greatly in how they enforce these laws, but penalties may include
                                    fines.
                                </div>
                                <br/>
                                <div>
                                    To help you navigate your own local rules and regulations around RnR hosting, here are some resources to
                                    help you learn about the requirements for your area.
                                </div>
                                <br/>
                                <Row>
                                    <Col>
                                        <div className="host-body-1-col2-sub">
                                            Check RnR's Help Center
                                        </div>
                                        <div>
                                            Although RnR does not provide specific legal advice, you can find general 
                                            <strong>guidelines on navigating local laws</strong> in the Help Center. To 
                                            help you find information about safety and regulatory considerations for your 
                                            area, RnR has also compiled <strong>responsible hosting</strong> resources 
                                            for a number of geographies around the world
                                        </div>
                                    </Col>
                                    <Col>
                                        <div className="host-body-1-col2-sub">
                                            Reach out to other hosts
                                        </div>
                                        <div>
                                            Connect with hosts in your community by joining your local <strong>host club.</strong> 
                                            Host clubs are host-led online groups that provide a space for you to connect 
                                            with other hosts in your area. These hosts are likely to know about your local 
                                            requirements, and can often provide tips on how to address them. RnR's 
                                            <strong>Community Center</strong> is also an excellent place to connect with 
                                            seasoned hosts willing to answer questions and share their hosting experiences.
                                        </div>
                                    </Col>
                                </Row>
                                <br/>
                                <Row>
                                    <Col>
                                        <div className="host-body-1-col2">
                                            Learn about RnR's City Portal
                                        </div>
                                        <br/>
                                        <div>
                                            To continue to grow local partnerships and support healthy tourism, we launched 
                                            City Portal, a first-pf-its-kinds resource to help governments. <strong>City Portal</strong> 
                                            provides relevant insights related to RnR's activity, tools to help serve local communities, 
                                            and access to resources and support.
                                        </div>
                                        <br/>
                                        <div>
                                            This resource is an important next step in strengthening our relationships with 
                                            communities, with the goal of ultimately protecting the future of home sharing 
                                            for hosts and guests on RnR.
                                        </div>
                                    </Col>
                                    <Col>
                                        <div className="host-body-1-col2">
                                            Start hosting responsibly
                                        </div>
                                        <br/>
                                        <div>
                                            Before you can start welcoming guests into your place, the first step is to 
                                            make sure your listing complies with local regulations, permit requirements, and 
                                            tax agreements. By looking into which local rules and regulations apply to you, 
                                            you're one step closer to earning money and hosting responsibly.
                                        </div>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </div>

                    {/* Line break */}
                    <hr size="8" width="90%" color="red"/>
                    
                    <div className="host-body-container-1">
                        <h1 className="host-body-title">How do we support you?</h1>
                        <br/>
                        <Row>
                            <Col>
                                <div className="host-body-1-col2">
                                    Host protection programs
                                </div>
                                <br/>
                                <div>
                                    To support you in the rare event of an incident, most RnR bookings include property 
                                    damage protection and liability insurance of up to $1M USD.
                                </div>
                                <br/>
                                <div>
                                    <strong><a href="">How you're protected while hosting</a></strong>
                                </div>
                            </Col>
                            
                            <Col>
                                <div className="host-body-1-col2">
                                    Covid-19 safety guidelines
                                </div>
                                <br/>
                                <div>
                                    To help protect the health of our community, we have partnered with experts to create 
                                    safety practices for everyone, plus a cleaning process for hosts.
                                </div>
                                <br/>
                                <div>
                                    <strong><a href="">Get to know the enhanced cleaning process</a></strong>
                                </div>
                            </Col>
                            
                            <Col>
                                <div className="host-body-1-col2">
                                    High guest standards
                                </div>
                                <br/>
                                <div>
                                    To give Host peace of mind, we offer guest identification and let you check out reviews 
                                    of guests before they book. Our new Guest Standards Policy sets higher expectations 
                                    for behavior.
                                </div>
                                <br/>
                                <div>
                                    <strong><a href="">Steps we take to help Hosts feel confident</a></strong>
                                </div>
                            </Col>
                        </Row>    
                    </div>
                </div>
                <FooterMenu/>
            </div>
        )
    }
}