import React, { Component } from 'react';
import {Modal, Button, Row, Col, Form} from 'react-bootstrap';
import '../../custom.css';

export default class Registration extends Component {
    static displayName = Registration.name;
    
    constructor(props) {
        super(props);
    }
    
    render(){
        return (
            <Modal
                {...this.props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    {/*<Modal.Title id="contained-modal-title-vcenter">*/}
                    {/*    RnR Registration*/}
                    {/*</Modal.Title>*/}
                </Modal.Header>
                <Modal.Body>
                    <h2 className="registration-title">RnR Registration</h2>
                    <br/>
                    <Form>
                        <Form.Group controlId="exampleForm.ControlInput1">
                            <Form.Row>
                                <Col>
                                    <Form.Label><strong className="registration-label">First Name</strong></Form.Label>
                                    <Form.Control type="text" placeholder="First Name" />
                                </Col>
                                <Col>
                                    <Form.Label><strong className="registration-label">Last Name</strong></Form.Label>
                                    <Form.Control type="text" placeholder="Last Name" />
                                </Col>
                            </Form.Row>
                        </Form.Group>
                        <Form.Group controlId="exampleForm.ControlInput1">
                            <Form.Row>
                                <Col>
                                    <Form.Label><strong className="registration-label">Date of Birth</strong></Form.Label>
                                    <Form.Control type="text" placeholder="DOB" />
                                </Col>
                                <Col>
                                    <Form.Label><strong className="registration-label">Phone Number</strong></Form.Label>
                                    <Form.Control type="text" placeholder="(xxx) xxx xxxx" />
                                </Col>
                            </Form.Row>
                        </Form.Group>
                        <Form.Group controlId="exampleForm.ControlInput1">
                            <Form.Label><strong className="registration-label">Username</strong></Form.Label>
                            <Form.Control type="text" placeholder="Username (alalalong)" />
                        </Form.Group>
                        <Form.Group controlId="exampleForm.ControlInput1">
                            <Form.Label><strong className="registration-label">Password</strong></Form.Label>
                            <Form.Control type="text" placeholder="Application Password" />
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="outline-danger" onClick={this.props.onHide} block>Submit</Button>
                    <Button variant="light" onClick={this.props.onHide} block>Close</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}