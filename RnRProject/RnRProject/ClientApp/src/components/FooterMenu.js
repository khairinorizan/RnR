import React, { Component } from 'react';
import {Link} from "react-router-dom";

export default class FooterMenu extends Component{
    static displayName = FooterMenu.name;

    constructor(props) {
        super(props);
    }
    
    render(){
        return(
            <div className="footer-box">
                {/*<h1 style={{ color: "green",*/}
                {/*    textAlign: "center",*/}
                {/*    marginTop: "-50px" }}>*/}
                {/*    GeeksforGeeks: A Computer Science Portal for Geeks*/}
                {/*</h1>*/}
                <div className="footer-container">
                    <div className="footer-container-row">
                        <div className="footer-container-column">
                            <div className="footer-heading">ABOUT</div>
                            <div className="footer-link">
                                <Link to="/HowRnRWorks">How RnR works</Link>
                            </div>
                            <div className="footer-link">
                                <a href="#">Newsroom</a>
                            </div>
                            <div className="footer-link">
                                <a href="#">Investors</a>
                            </div>
                            <div className="footer-link">
                                <a href="#">RnR Plus</a>
                            </div>
                            <div className="footer-link">
                                <a href="#">RnR Luxe</a>
                            </div>
                            <div className="footer-link">
                                <a href="#">HotelTonight</a>
                            </div>
                            <div className="footer-link">
                                <a href="#">RnR for Work</a>
                            </div>
                            <div className="footer-link">
                                <a href="#">Made possible by Host</a>
                            </div>
                            <div className="footer-link">
                                <a href="#">Careers</a>
                            </div>
                            <div className="footer-link">
                                <a href="#">Founders' Letter</a>
                            </div>
                        </div>

                        <div className="footer-container-column">
                            <div className="footer-heading">COMMUNITY</div>
                            <div className="footer-link">
                                <a href="#">Diversity & Belonging</a>
                            </div>
                            <div className="footer-link">
                                <a href="#">Against Discrimination</a>
                            </div>
                            <div className="footer-link">
                                <a href="#">Accessibility</a>
                            </div>
                            <div className="footer-link">
                                <a href="#">RnR Associates</a>
                            </div>
                            <div className="footer-link">
                                <a href="#">Frontline Stays</a>
                            </div>
                            <div className="footer-link">
                                <a href="#">Guest Referrals</a>
                            </div>
                            <div className="footer-link">
                                <a href="#">Gift cards</a>
                            </div>
                            <div className="footer-link">
                                <a href="#">RnR.org</a>
                            </div>
                        </div>

                        <div className="footer-container-column">
                            <div className="footer-heading">HOST</div>
                            <div className="footer-link">
                                <a href="#">Host your home</a>
                            </div>
                            <div className="footer-link">
                                <a href="#">Host an Online Experience</a>
                            </div>
                            <div className="footer-link">
                                <a href="#">Host an Experience</a>
                            </div>
                            <div className="footer-link">
                                <a href="#">Responsible hosting</a>
                            </div>
                            <div className="footer-link">
                                <a href="#">Resource Center</a>
                            </div>
                            <div className="footer-link">
                                <a href="#">Community Center</a>
                            </div>
                        </div>
                        <div className="footer-container-column">
                            <div className="footer-heading">SUPPORT</div>
                            <div className="footer-link">
                                <a href="#">Our COVID-19 Response</a>
                            </div>
                            <div className="footer-link">
                                <a href="#">Help Center</a>
                            </div>
                            <div className="footer-link">
                                <a href="#">Cancellation options</a>
                            </div>
                            <div className="footer-link">
                                <a href="#">Neighborhood Support</a>
                            </div>
                            <div className="footer-link">
                                <a href="#">Trust & Safety</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}