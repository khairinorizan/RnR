import React, { Component } from 'react';
import { Collapse, Container, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from 'reactstrap';
import { Link } from 'react-router-dom';
import './NavMenu.css';

// Import registration popup
import Registration from './Registration/Registration';

export class NavMenu extends Component {
  static displayName = NavMenu.name;

  constructor (props) {
    super(props);

    this.toggleNavbar = this.toggleNavbar.bind(this);
    
    this.state = {
      collapsed: true,
      addModalShow: false
    };
  }

  toggleNavbar () {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }

  render () {
    let addModalClose = () => this.setState({addModalShow: false});
    return (
        <div>
          <header>
            <Navbar className="navbar-expand-sm navbar-toggleable-sm" style={{backgroundColor:"rgb(220, 152, 138)", borderColor: "rgb(207, 142, 130)"}} light>
              <Container>
                <NavbarBrand tag={Link} to="/">RnR</NavbarBrand>
                <NavbarToggler onClick={this.toggleNavbar} className="mr-2" />
                <Collapse className="d-sm-inline-flex flex-sm-row-reverse" isOpen={!this.state.collapsed} navbar>
                  <ul className="navbar-nav flex-grow" style={{fontFamily: "'PT Sans Narrow', sans-serif"}}>
                    <NavItem>
                      <NavLink tag={Link} className="text-dark" to="/">Market</NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink tag={Link} className="text-dark" to="/Host/Home">Become a Host</NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink className="text-dark" onClick={() => this.setState({addModalShow: true})}>Registration</NavLink>
                    </NavItem>
                  </ul>
                </Collapse>
              </Container>
            </Navbar>
          </header>
          {/* Show popup here */}
          <Registration
              className="modal-popup"
              show={this.state.addModalShow}
              onHide={addModalClose}
          />
        </div>
    );
  }
}
